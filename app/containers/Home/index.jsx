import React from 'react'
import {connect} from 'react-redux'

import HomeHeader from '../../components/HomeHeader'
import List from './subpage/List'

class Home extends React.Component {
    render() {
        return (
            <div>
                <HomeHeader cityName={this.props.userinfo.cityName}/>
                <img src="/static/images/bearw.jpg" alt=""/>
                <List cityName={this.props.userinfo.cityName}/>
            </div>
        )
    }
}

// -------------------redux react 绑定--------------------

function mapStateToProps(state) {
    return {
        userinfo: state.userinfo
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

