import React from 'react'
import { Link } from 'react-router-dom'

class List extends React.Component {
    render() {
        const arr = [1, 2, 3]
        return (
            <div>
              <ul>
                  {arr.map((item, index) => {
                      return <Link key={index} to={ `/detail/${item}` }><li>js jump to {item}</li></Link>
                  })}
              </ul>
            </div>
        )
    }
}

export default List