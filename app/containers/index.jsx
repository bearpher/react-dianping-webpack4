import React from 'react'
import history from '../util/history'
import {Router, Route, Switch} from 'react-router-dom'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import LocalStore from '../util/localStore'
import { CITYNAME } from '../config/localStoreKey'
import * as userInfoActionsFromOtherFile from '../actions/userinfo'

import Home from './Home/index'
import City from './City/index'
import List from './List/index'
import Detail from './Detail/index'
import NotFound from './NotFound/index'

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            initDone: false
        }
    }

    componentDidMount() {
        // 获取位置信息
        let cityName = LocalStore.getItem(CITYNAME)
        if (cityName == null) {
            cityName = '北京'
        }
        this.props.userInfoActions.update({
            cityName: cityName
        })

        // 更改状态
        this.setState({
            initDone: true
        })
    }

    render() {
        return (
            <Router history={history}>
                {
                    this.state.initDone
                        ? this.renderLinks()
                        : <div>正在加载...</div>
                }
            </Router>
        )
    }

    renderLinks() {
        return (
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path='/city' component={City}/>
                <Route path='/list' component={List}/>
                <Route path='/detail/:id' component={Detail}/>
                <Route component={NotFound}/>
            </Switch>
        )
    }
}


// -------------------redux react 绑定--------------------

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userInfoActions: bindActionCreators(userInfoActionsFromOtherFile, dispatch),
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)