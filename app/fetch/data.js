import { get } from './get.js'
import { post } from './post.js'

export function getData() {
    // '/api/get' 获取字符串
    const result = get('/api/get')

    result.then(res => {
        return res.text()
    }).then(text => {
        console.log(text)
    })

}

export function postData() {
    // '/api/post' 提交数据
    const result = post('/api/post', {
        a: 100,
        b: 200
    })

    result.then(res => {
        return res.json()
    }).then(json => {
        console.log(json)
    })
}