import React from 'react'
import { render } from 'react-dom'
import App from './containers'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'

// 通用样式
import './static/css/common.less'
import './static/css/fonts.css'

const store = configureStore()

// 测试 fetch 的功能
import { getData, postData } from './fetch/data.js'
getData();
postData();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)