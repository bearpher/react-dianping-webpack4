const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-body-parser');
const faker = require('faker/locale/zh_CN');

const app = new Koa();
const router = new Router();

router.get('/api/get', (ctx, next) => {
    // console.log(ctx)
    // ctx.body = `Request Body: ${JSON.stringify(ctx.request.body)}`;
    ctx.body = {
        info: 'get测试数据',
        name: faker.name.firstName() + faker.name.lastName(),
        image: faker.image.avatar(),
        words: faker.lorem.words(),
        url: ctx.request.url
    }
});


router.post('/api/post', (ctx, next) => {
    ctx.body = {
        name: 'post测试数据',
        body: ctx.request.body,
        url: ctx.request.url
    }
});

// 首页 —— 推荐列表（猜你喜欢）
var homeListData = require('./home/list.js')
router.get('/api/homelist/:city/:page', (ctx, next) => {
    ctx.body = homeListData
});

//这句use放在前面
app.use(bodyParser());

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(8002);
