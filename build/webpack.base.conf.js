const path = require('path');
const APP_PATH = path.resolve(__dirname, '../app');

module.exports = {
  entry: {
    app: './app/index.jsx',
    framework: ['react', 'react-dom'],
  },

  resolve:{
    extensions:['.js','.jsx']
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        include: APP_PATH
      },
      {
        test: /\.(png|gif|jpg|jpeg|bmp)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 5000
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              name: "fonts/[name].[ext]",
              limit: 5000
            }
          }
        ]
      }
    ]
  }
};