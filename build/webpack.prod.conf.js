const path = require('path');
const config = require('../config');
const baseWebpackConfig = require('./webpack.base.conf');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = merge(baseWebpackConfig, {
  mode: 'production',
  output: {
    path: config.build.assetsRoot,
    publicPath: config.build.assetsPublicPath,
    filename: path.posix.join(config.build.assetsSubDirectory, 'js/[name].[chunkhash:16].js'),
    chunkFilename: path.posix.join(config.build.assetsSubDirectory, 'js/[name].[chunkhash].js'),
  },
  module: {
    rules:
      [
        {
          test: /\.(css)$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
                localIdentName: '[local]'
              }
            },
            {
              loader: 'postcss-loader'
            }
          ]
        },
        {
          test: /\.(less)$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
                localIdentName: '[local]'
              }
            },
            {
              loader: 'postcss-loader'
            },
            {
              loader: 'less-loader'
            }
          ]
        }
      ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      inject: 'body',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      },
    }),
    new CleanWebpackPlugin([config.build.assetsRoot], { allowExternal: true }),

    //导出css
    new MiniCssExtractPlugin({
      filename: path.posix.join(config.build.assetsSubDirectory, 'css/[name].[hash].css'),
      chunkFilename: path.posix.join(config.build.assetsSubDirectory, 'css/[id].[hash].css'),
    }),
  ],
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: true
          ? {
            map: { inline: false }
          }
          : {}
      })
    ],
    splitChunks: {
      chunks: "all",
      minChunks: 1,
      cacheGroups: {
        framework: {
          priority: 200,
          test: "framework",
          name: "framework",
          enforce: true,
          reuseExistingChunk: true
        },
        vendor: {
          priority: 10,
          test: /node_modules/,
          name: "vendor",
          enforce: true,
          reuseExistingChunk: true
        }
      }
    }
  }
});